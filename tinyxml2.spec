Name:           tinyxml2
Version:        9.0.0
Release:        2
Summary:        C++ XML parser
License:        zlib
URL:            https://github.com/leethomason/%{name}
Source0:        https://github.com/leethomason/%{name}/archive/refs/tags/%{name}-%{version}.tar.gz
BuildRequires:  gcc-c++ cmake

%description
TinyXML-2 is a simple, small, efficient, C++ XML parser that can be
easily integrated into other programs.  TinyXML-2 parses an XML document, and builds 
from that a Document Object Model (DOM) that can be read, modified, and saved.

%package        devel
Summary:        Development files for tinyxml2
Requires:       %{name} = %{version}-%{release}
%description    devel
The devel package contains development files for tinyxml2.It provides
header files and libraries for tinyxml2.

%prep
%autosetup -n %{name}-%{version}
chmod -c -x *.cpp *.h
sed -i -e 's,lib/,${CMAKE_INSTALL_LIBDIR}/,g' CMakeLists.txt

%build
%cmake -DBUILD_STATIC_LIBS=OFF
%cmake_build

%check
%ctest

%install
%cmake_install

%files
%license LICENSE.txt
%doc readme.md
%{_libdir}/lib%{name}.so.*

%files devel
%{_includedir}/%{name}.h
%{_libdir}/lib%{name}.so
%{_libdir}/pkgconfig/%{name}.pc
%{_libdir}/cmake/%{name}

%changelog
* Thu Dec 12 2024 Funda Wang <fundawang@yeah.net> - 9.0.0-2
- adopt to new cmake macro

* Wed Jun 07 2023 chenchen <chen_aka_jan@163.com> - 9.0.0-1
- Upgrade to 9.0.0

* Thu Nov 21 2019 zhujunhao <zhujunhao5@huawei.com> - 6.0.0-5
- Initial package.
